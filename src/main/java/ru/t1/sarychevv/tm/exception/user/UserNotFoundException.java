package ru.t1.sarychevv.tm.exception.user;

import ru.t1.sarychevv.tm.exception.entity.AbstractEntityNotFoundException;

public final class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
