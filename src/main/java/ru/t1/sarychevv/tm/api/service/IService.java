package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.repository.IRepository;
import ru.t1.sarychevv.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
