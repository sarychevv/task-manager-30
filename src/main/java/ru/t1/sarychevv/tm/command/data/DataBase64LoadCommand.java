package ru.t1.sarychevv.tm.command.data;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.Domain;
import ru.t1.sarychevv.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from BASE64 file";

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    @Getter
    @Setter
    private String file_name = FILE_BASE64;

    @SneakyThrows
    @Override
    public void execute() {
        if (getFile_name().equals(FILE_BASE64)) System.out.println("[DATA BASE64 LOAD]");
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(getFile_name()));
        @Nullable final String base64Date = new String(base64Byte);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Date);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
